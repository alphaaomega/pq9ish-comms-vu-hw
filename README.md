# PQ9ISH V/U COMMS/OBC Hardware

A PocketQube format main board around STM32 with AX5043 transceiver utlizing the modified PQ9 standard.

Read hardware configuration before assembly.

## RF
AX5043 COMMS Transceiver with PA and dedicated RF lines for RX and TX

Modulations:  
FSK, MSK, 4-FSK, GFSK, GMSK, AFSK and ASK  

## Storage
MicroSD Slot on 4-bit bus

## Connectivity

Subsystem communication over CAN Bus  
Up to 1Mbps transfer rate.  
Posibillity for single wire driverless CAN Bus for short bus length.

Auxiliary RS232/RS485

USB connection

Programm/Debug via SWD port

# Hardware configuration

## v0.9.2
### Power configuration
There are is a combination of power options for this board. 

|Setup|Description|JP1|JP2|Notes
|-----|-----------|--|--|-|
|Dual Voltage, internal 3.3V|5V Input on V4, 3.3V via internal regulator. Maximum RF power|Open|Short 1-2|
|Dual Voltage, external 3.3V|5V Input on V4, 3.3V on V1. Maximum RF power|Short|Short 1-2|Do not populate U10
|Single Voltage 3.3V|3,3V on V1, reduced RF power|Short|Short 2-3|Do not populate U10

Backup battery for RTC, 3V CR2012 or equivalent
### CAN Bus configuration

|Driver|Populate|Do not populate|JP3, JP4|
|-|-|-|-|
|YES|U6, C39, R9[1]|D1, R10|Open|
|NO|D1, R10[2]|U6, C39, R9|Short|

[1] R9 120 Ohm resistor should be present on the outmost subsystems. Populate accordingly depending on your subsystem configuration  
[2] Bus pull-up. Should be present on a single subsystems

### RF Configuration
This board is configured for Rx/Tx on UHV. Filters and matching are around 433MHz.

For VHF band populate L-VCO1. Additional component changes for impedance matching may be needed.

## v0.9.1
TBC
## v0.9
Initial concept

# Contribute

Schematic and PCB are designed on KiCAD v5.1  

Clone the repository with --recursive to make sure lsf-kicad-lib submodule is initialiazed.

View this project on [CADLAB.io](https://cadlab.io/project/1787). 

# License

Licensed under the [CERN OHLv1.2](LICENSE) 2018 Libre Space Foundation.

STEP files:

Micro-USB connector by Amphenol
JST-SH SMD connectors from grabcad by Denis Silivanov https://grabcad.com/library/jst-sh-smd-connectors-1
